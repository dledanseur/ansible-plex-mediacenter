# ansible-plesk-mediacenter

Ansible role to install plesk mediacenter on ubuntu

Usage:

```
#!yaml

hosts: my.host.example.com
roles:
  - dledanseur.plesk-mediacenter
    vars:
      plesk_mediacenter_version: 1.0.0.2261-a17e99e

```
